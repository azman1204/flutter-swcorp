// string
void main() {
  // multiple lines string. output juga multiple lines
  // jika guna + utk multiple lines, output tidak multiple
  var str = '''
      Line 1
      Line 2
      Line 3
      ''';
  print(str);
  var name = 'John Doe';
  var age = 40;
  print("Name = " + name + " age = " + age.toString());
}
