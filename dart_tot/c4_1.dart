class Person {
  String? firstName;
  String lastName = "Doe";
  // this property own by class Person, not object of person
  static String label = "Name : "; // static property

  // lambda
  String getFullName() => "$firstName $lastName";

  String getUpperName() {
    return "$firstName $lastName".toUpperCase();
  }

  // static member x boleh access non-static member
  static String printPerson(Person p) {
    return "$label ${p.firstName} ${p.lastName}";
  }
}

void main() {
  Person somePerson = Person();
  somePerson.firstName = "John";
  print(Person.label + " " + somePerson.getFullName());
  print(somePerson.getUpperName());
  print(Person.printPerson(somePerson));
}
