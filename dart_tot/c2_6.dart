// function
// String = return type. jika xmention return type, akan return null
// void = x return apa2
String sayHello() {
  return "Hello World";
  //return 123; // error
}

// jika parameter ada default value "?" x wajib
// "?" - boleh null
String sayHappyBirthday(String name, [int? age = 0]) {
  return "$name is $age years old";
}

// named parameter vs positional parameter
// named parameter is optional by default, kalau nak wajibkan kena letak "required"
// postional first, then named parameter
// wajib param, baru optional
String sayHappyBirthday2(String name,
    {required int age, double bonus = 5000.00}) {
  return "Happy birthday to $name. You are $age years old";
}

void main() {
  print(sayHello());
  print(sayHappyBirthday("John Doe", 40));
  print(sayHappyBirthday("Ali"));
  print(sayHappyBirthday2("Abu", bonus: 8000.00, age: 45));
}
