// map {key : value}
void main() {
  var student = {"name": "john Doe", "age": 18};
  // dlm dart "" dan '' adalah sama
  var name = "Abu";
  student["matrix"] = 1234;
  print("Nama ${name}");
  print("Name = ${student['name']} matrix = ${student['matrix']}");

  var pelajar = [
    {"name": "john", "age": 18},
    {"name": "abu", "age": 19}
  ];
  var stu1 = pelajar[0];
  print("student 1 : \n name : ${stu1['name']} \n age: ${stu1['age']}");

  var stu2 = pelajar[1];
  print("Maklumat student 2:" +
      "\n name : ${stu2['name']}" +
      "\n age  : ${stu2['age']}");
}
