// loop
void main() {
  // while loop
  int i = 1;
  while (i <= 10) {
    print("i = $i");
    i++;
  }

  // while with break
  int k = 1;
  while (k <= 10) {
    print("k = $k");
    if (k == 5) break; // exit loop
    k++;
  }

  // while with continue
  int m = 0;
  while (m <= 10) {
    m++;
    if (m % 2 == 0) continue; // skip next stmt, but run next loop
    print("m = $m");
  }

  // for loop
  for (int j = 1; j <= 10; j++) {
    print("j = $j");
  }

  // loop dalam loop
  for (int j = 0; j <= 5; j++) {
    for (k = 0; k <= 5; k++) {
      print("($j, $k)");
    }
  }

  // foreach loop - khas utk list
  List numbers = [1, 2, 3, 4, 5];
  for (int num in numbers) {
    print("num = $num");
  }

  numbers.forEach((element) {
    print("num2 = $element");
  });
}
