// List (array dlm php)
// dlm dart semua benda adalah object
void main() {
  // var names = ["Azman", "John Doe", "Abu Bakar"];
  List names = ["Azman", "John Doe", "Abu Bakar"];
  print(names[1]); // John Doe
  print("bil data : ${names.length}"); // $names->length (php)
  names.add("Ali");
  names[1] = "Jane Doe"; // update value
  print(names);
}
