import 'person.dart';

class Student extends Person {
  var matrix = 1000;
  var cgpa = 3.5;

  // child akan panggil parent constructor
  Student(int matrix, double cgpa, String fname, String lname)
      : super(fname, lname) {
    // "this" guna bila nama property dan parameter sama
    this.matrix = matrix;
    this.cgpa = cgpa;
  }

  @override
  String toString() {
    return "Name : $firstName $lastName \n" +
        "Matrix : $matrix \n" +
        "CGPA : $cgpa";
  }

  @override
  String fullName() {
    return "$firstName - $lastName";
  }
}
