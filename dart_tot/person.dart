class Person {
  var firstName;
  var lastName;

  // constructor. nama method ini sama dengan nama class. tiada return type
  // auto run bila create obj dr class ini
  // constructor guna utk initialize property value
  // dlm satu class hanya boleh ada satu constructor
  // Person() {
  //   print("running constructor");
  // }

  // Person(String fName, [String? lName = '']) {
  //   firstName = fName;
  //   lastName = lName;
  // }

  // shortcut
  Person(this.firstName, this.lastName); // sama dgn yg atas

  String fullName() {
    return "$firstName $lastName";
  }
}
