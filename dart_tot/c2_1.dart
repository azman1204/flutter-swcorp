void main() {
  // data type: int, double, bool, String, List, Map
  print("Hello World");
  String nama = "Azman";
  var nama2 = "John Doe";
  var umur = 40; // optional int
  int umur2 = 41;
  int age; // wajib int
  double gaji = 7000.50;
  print("nama = $nama"); // inject var dlm string (interpolation)
  print("2 + 2 = ${2 + 2}");
}
