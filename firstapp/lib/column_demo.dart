import 'package:flutter/material.dart';

class ColumnDemo extends StatelessWidget {
  const ColumnDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Colum Demo"),
          backgroundColor: Colors.teal[400],
          leading: Icon(Icons.heart_broken, color: Colors.red,),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            IconButton(onPressed: (){}, icon: Icon(Icons.car_crash, size: 40.0,)),
            IconButton(onPressed: (){}, icon: Icon(Icons.train, size: 40.0,)),
          ],
        ),
      ),
    );
  }
}
