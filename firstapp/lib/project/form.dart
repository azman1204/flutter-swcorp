import 'package:flutter/material.dart';

class MyForm extends StatefulWidget {
  const MyForm({super.key});

  @override
  State<MyForm> createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  var nama = "John Doe";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          children: [
            TextField(
              onChanged: (val) {
                setState(() {
                  nama = val;
                });
              },
            ),
            ElevatedButton(
              onPressed: (){},
              child: Text("Submit")
            ),
            Text("Data = $nama")
          ],
        ),
      )
    );
  }
}
