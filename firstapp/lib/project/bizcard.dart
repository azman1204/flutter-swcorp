import 'package:flutter/material.dart';

class Bizcard extends StatelessWidget {
  const Bizcard({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.teal[500],
          body: Column(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage('images/azmn.png'),
                radius: 50,
              ),
              SizedBox(height: 40,),
              Text("Azman bin Zakaria", style: TextStyle(
                fontFamily: "IndieFlower",
                fontWeight: FontWeight.bold,
                fontSize: 40
              ),),
              Text("Mobile App Developer", style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.phone),
                  Text(" : 016 - 2370 394"),
                  SizedBox(width: 40,),
                  Icon(Icons.email),
                  Text(" : azman1204@yahoo.com"),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
