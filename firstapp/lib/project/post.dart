import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Post extends StatefulWidget {
  const Post({super.key});

  @override
  State<Post> createState() => _PostState();
}

class _PostState extends State<Post> {
  List<dynamic> jsonResponse = [];
  // initSate() fungsi seperti constructor
  @override
  void initState() {
    super.initState();
    print("initializing....");
    getData();
  }

  void getData() async {
    // get data from https://jsonplaceholder.typicode.com/posts
    var url = Uri.https('jsonplaceholder.typicode.com', '/posts');
    var response = await http.get(url); // asynchronous request
    if (response.statusCode == 200) {
      // ok
      setState(() {
        jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
      });
    } else {
      print("Ada masalah...");
    }
  }

  List<Widget> getCard() {
    List<Widget> lists = [];
    for(int i=0; i<jsonResponse.length; i++) {
      var data = jsonResponse[i]['title'];
      lists.add(Card(
        child: ListTile(
          title: Text(data),
        ),
      ));
    }
    return lists;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: ListView(
          children: getCard(),
        ),
      ),
    );
  }
}

// Map<int, int> numbers = {1: 10, 2: 20};
// Map<String, int> numbers2 = {"satu": 10, "dua": 20};
// Map<dynamic, dynamic> numbers3 = {"satu": 10, 2: 20};
