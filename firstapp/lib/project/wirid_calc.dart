import 'package:flutter/material.dart';

class WiridCalc extends StatefulWidget {
  const WiridCalc({super.key});

  @override
  State<WiridCalc> createState() => _WiridCalcState();
}

class _WiridCalcState extends State<WiridCalc> {
  var no = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("WIRID CALCULATOR"),
          backgroundColor: Colors.teal[400],
          centerTitle: true,
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/mesjid.jpg'),
              fit: BoxFit.cover
            )
          ),
          child: Center(
            child: Text("$no", style: TextStyle(
              fontSize: 180,
              fontFamily: "IndieFlower",
              shadows: <Shadow>[
                Shadow(
                  offset: Offset(5.0, 5.0),
                  blurRadius: 3.0,
                  color: Colors.teal
                )
              ]
            ),),
          ),
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(left: 30),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: (){
                  setState(() {
                    no++;
                  });
                }
              ),
              Expanded(child: Container()),
              FloatingActionButton(
                child: Icon(Icons.clear),
                onPressed: (){
                  setState(() {
                    no = 0;
                  });
                }
              )
            ],
          ),
        ),
      ),
    );
  }
}
