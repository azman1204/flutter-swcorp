import 'package:flutter/material.dart';

class StatefulDemo extends StatefulWidget {
  const StatefulDemo({super.key});

  @override
  State<StatefulDemo> createState() => _StatefulDemoState();
}

class _StatefulDemoState extends State<StatefulDemo> {
  // state / property dynamic
  var name = "John Doe";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Stateful Widget Demo"),),
        body: Center(
          child: Text("Name : $name"),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.plumbing),
          onPressed: () {
            print("You clicked me!");
            setState(() {
              if (name == "John Doe") {
                name = "Abu Bakar Ellah";
              } else {
                name = "John Doe";
              }
            });
          },
        ),
      ),
    );
  }
}
