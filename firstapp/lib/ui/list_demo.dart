import 'package:flutter/material.dart';

class ListDemo extends StatelessWidget {
  const ListDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.lime[300],
          body: ListView(
            children: getContainer2(),
          ),
        ),
      ),
    );
  }

  List<Widget> getContainer2() {
    List<Widget> cards = [];
    for(int i=1; i<=100; i++) {
      cards.add(Card(
        child: ListTile(
          leading: Icon(Icons.headphones),
          title: Text("Surah No : $i"),
          subtitle: Text("Bil ayat : 70"),
          trailing: Icon(Icons.delete, color: Colors.red,),
        ),
      ));
    }
    return cards;
  }

  List<Widget> getContainer() {
    List<Widget> containers = [];
    for (int i = 1; i <= 100; i++) {
      containers.add(
        Container(
          height: 50,
          color: Colors.blue[100],
          margin: EdgeInsets.all(10),
          child: Text("Nombor $i"),
        ),
      );
    }
    return containers;
  }
}
