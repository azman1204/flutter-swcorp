import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.teal,
          appBar: AppBar(
            title: const Text("My First App"),
            backgroundColor: Colors.blue,
          ),
          body: Center(
            child: Column(
              children: [
                Text("Hello World",
                  style: TextStyle(
                      color: Colors.pink,
                      fontSize: 30.0,
                      decoration: TextDecoration.none
                  ),
                ),
                TextButton(
                  onPressed: (){
                    Navigator.pushNamed(context, '/calc');
                  },
                  child: Text("Go to Wirid Calculator")
                ),
                SpinKitRing(
                  color: Colors.pinkAccent,
                  size: 150.0,
                )
              ],
            ),
          ),
        )
    );
  }
}
