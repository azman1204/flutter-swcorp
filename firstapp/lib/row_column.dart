import 'package:flutter/material.dart';

class RowColumn extends StatelessWidget {
  const RowColumn({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Row and Column"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 200.0,
                  height: 200.0,
                  color: Colors.lime,
                ),
                SizedBox(
                  width: 20.0,
                ),
                Container(width: 200.0, height: 200.0, color: Colors.lime),
                SizedBox(
                  width: 40.0,
                ),
                Container(width: 200.0, height: 200.0, color: Colors.lime),
              ],
            ),
            SizedBox(
              height: 40.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(width: 200.0, height: 200.0, color: Colors.blue),
                Container(width: 200.0, height: 200.0, color: Colors.blue),
              ],
            )
          ],
        ),
      ),
    );
  }
}
