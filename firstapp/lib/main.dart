import 'package:firstapp/home.dart';
import 'package:firstapp/project/bizcard.dart';
import 'package:firstapp/project/form.dart';
import 'package:firstapp/project/post.dart';
import 'package:firstapp/project/wirid_calc.dart';
import 'package:firstapp/row_column.dart';
import 'package:firstapp/ui/burung.dart';
import 'package:firstapp/ui/list_demo.dart';
import 'package:firstapp/ui/stateful_demo.dart';
import 'column_demo.dart';
import 'row_demo.dart';
import 'package:flutter/material.dart';

void main() {
  // Person("John")
  //runApp(Home());
  // runApp(RowDemo());
  // runApp(ColumnDemo());
  // runApp(RowColumn());
  // runApp(Burung());
  // runApp(ListDemo());
  // runApp(Bizcard());
  // runApp(StatefulDemo());
  // runApp(WiridCalc());
  runApp(MaterialApp(
    initialRoute: '/form',
    routes: {
      '/' : (context) => Home(),
      '/calc' : (context) => WiridCalc(),
      '/post' : (context) => Post(),
      '/form' : (context) => MyForm(),
    },
  ));
}